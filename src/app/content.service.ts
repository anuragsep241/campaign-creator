import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class ContentService {
  constructor(private http: HttpClient) {}

  /**
   * Get the content json
   */
  getContent(): Observable<Array<any>> {
    return this.http.get<any>(`assets/content.json`);
  }
}
