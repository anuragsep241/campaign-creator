import { mapCss } from './css-creator';

const parent = document.createElement('div');

describe('map Css should ', () => {
    it('map all the css in a element ', () => {
        mapCss(parent, 'text', 'this is the example text');
        mapCss(parent, 'contentType', 'this is the example text');
        mapCss(parent, 'headerType', 'this is the example text');
        mapCss(parent, 'image', 'this is the example text');
        mapCss(parent, 'placeholder', 'this is the example text');
        mapCss(parent, 'textLimit', '300');
        expect(parent.innerText).toBe('this is the example text');
    });
});
