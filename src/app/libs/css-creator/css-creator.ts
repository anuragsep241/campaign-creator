
export const mapCss = (element: HTMLElement, key, value): HTMLElement => {
    switch (key) {
        case 'text': {
            element.innerHTML = value;
            break;
        }
        case 'contentType':
        case 'headerType':
        {
            break;
        }
        case 'image': {
            element.style.backgroundImage  = `url(${value})`;
            break;
        }
        case 'placeholder': {
            element.setAttribute('placeholder', value);
            break;
        }
        case 'textLimit': {
            element.setAttribute('textLimit', value);
            break;
        }
        default : {
            element.style[key] = value;
        }
    }
    return element;
};

export const addCss = (element: HTMLElement, props: any): HTMLElement => {
    Object.keys(props).forEach((key) => {
        if (typeof props[key] === 'object') {
            return;
        }
        element = mapCss(element, key, props[key]);
    });
    return element;
};

