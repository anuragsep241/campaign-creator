import { createButton, createElement } from './element-creator';

describe('createElemnt should creare ', () => {
    it(' button', () => {
        const content = {
            appearance: 'ff-button',
            props: {
              element: {
                content: {
                  payload: 'example_payload',
                  text: 'Submit'
                },
                appearance: 'blue',
                disabled: false
              }
            }
          };

        const parent = document.createElement('div');
        createElement(parent, content);
        expect(parent.childElementCount).toBe(2);
    });

    it(' button with border outline', () => {
        const content = {
            appearance: 'ff-button',
            props: {
              element: {
                content: {
                  payload: 'example_payload',
                  text: 'Submit'
                },
                appearance: 'blue outline',
                disabled: false
              }
            }
          };

        const parent = document.createElement('div');
        createElement(parent, content);
        expect(parent.childElementCount).toBe(2);
    });

    it(' card with child span', () => {
        const content = {
            appearance: 'ff-card',
            props: {
              image: `<%= 'special-week-image' %>`
            },
            content: [
              {
                appearance: 'ff-content',
                props: {
                  text: 'Title goes here',
                  contentType: 'content3',
                  fontWeight: 'bold',
                  textAlign: 'left'
                }
              }
            ]
          };

        const parent = document.createElement('div');
        createElement(parent, content);
        expect(parent.childElementCount).toBe(2);
    });

    it(' text area', () => {
        const content = {
            appearance: 'ff-text-area',
            props: {
              placeholder: 'Write your review here (optional)',
              textLimit: 500
            }
          };

        const parent = document.createElement('div');
        createElement(parent, content);
        expect(parent.childElementCount).toBe(2);
    });
});
