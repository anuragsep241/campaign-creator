export interface ElementType {
    appearance: string;
    props: any;
    content?: Array<ElementType>;
}
