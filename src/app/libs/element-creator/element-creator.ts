import { ElementType } from './element.model';
import { addCss } from '../css-creator';

export const createElement = (parent: HTMLElement, element: ElementType) => {
  let child: HTMLElement;
  switch (element.appearance) {
    case 'ff-header': {
      const type = element.props.headerType.charAt(
        element.props.headerType.length - 1
      );
      child = document.createElement(`h${type}`);
      break;
    }
    case 'ff-content': {
      const type = element.props.contentType.charAt(
        element.props.contentType.length - 1
      );
      child = document.createElement(`span`);
      break;
    }
    case 'ff-card': {
      child = document.createElement(`div`);
      child.classList.add('card');
      break;
    }
    case 'ff-button': {
      child = createButton(element);
      break;
    }
    case 'ff-text-area': {
        child = document.createElement(`textarea`);
        child.setAttribute('rows', '5');
        break;
      }
  }
  child = addCss(child, element.props);

  if (element.content && element.content.length > 0) {
    element.content.forEach( (content: ElementType) => {
        createElement(child, content);
    });
  }
  parent.appendChild(document.createElement('br'));
  parent.appendChild(child);
};



export const createButton = (element: ElementType): HTMLElement => {
    const child = document.createElement('input');
    child.setAttribute('type', 'button');
    child.setAttribute('disabled', `${element.props.element.disabled}`);
    child.setAttribute('value', `${element.props.element.content.text}`);
    if (element.props.element && element.props.element.appearance) {
        if (element.props.element.appearance.split(' ').length > 1){
            child.style.border = `1px solid ${element.props.element.appearance.split(' ')[0]}`;
        } else{
            child.style.backgroundColor = element.props.element.appearance;
            child.style.color = 'white';
        }
    }
    return child;
};
