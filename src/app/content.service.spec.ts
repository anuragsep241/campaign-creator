import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from  '@angular/common/http/testing';

import { ContentService } from './content.service';

const mockContent: any[] = [
    {
      content: [
        {
          appearance: 'ff-header',
          props: {
            text: 'Welcome to FastForward.ai!',
            headerType: 'heading5',
            fontWeight: 'normal'
          }
        },
        {
          appearance: 'ff-content',
          props: {
            text: 'Hope you enjoy our new experience',
            contentType: 'content3',
            fontWeight: 'normal'
          }
        },
      ]
    }
  ];


describe('ContentService', () => {
  let service: ContentService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(ContentService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return users', () => {
    service.getContent().subscribe(content => {
      expect(content.length).toBe(1);
      expect(content).toEqual(mockContent);
    });
    const req = httpMock.expectOne(`assets/content.json`);
    expect(req.request.method).toBe('GET');
    req.flush(mockContent);

  });
});
