import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContentHolderComponent } from './content-holder/content-holder.component';

const routes: Routes = [
  { path: '', redirectTo: '/campaign', pathMatch: 'full' },
  { path: 'campaign', component: ContentHolderComponent },
  { path: 'campaign/:user', component: ContentHolderComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
