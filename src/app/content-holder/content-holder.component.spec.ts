import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, getTestBed, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { ContentService } from '../content.service';

import { ContentHolderComponent } from './content-holder.component';

const mockContent: any[] = [
  {
    content: [
      {
        appearance: 'ff-header',
        props: {
          text: 'Welcome to FastForward.ai!',
          headerType: 'heading5',
          fontWeight: 'normal'
        }
      },
      {
        appearance: 'ff-content',
        props: {
          text: 'Hope you enjoy our new experience',
          contentType: 'content3',
          fontWeight: 'normal'
        }
      },
    ]
  }
];

class MockContentSerivce {
  getContent() {
    return of(mockContent);
  }
}

describe('ContentHolderComponent', () => {
  let component: ContentHolderComponent;
  let fixture: ComponentFixture<ContentHolderComponent>;

  beforeEach(async(() => {
    let injector;
    let service: ContentService;
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ ContentHolderComponent ],
      providers: [
        { provide: ContentService, useClass: MockContentSerivce }
      ]
    })
    .compileComponents();
    injector = getTestBed();
    service = injector.get(ContentService);
    fixture = TestBed.createComponent(ContentHolderComponent);
    component = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
