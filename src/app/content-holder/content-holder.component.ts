import {
  Component,
  ElementRef,
  Inject,
  OnInit,
  Renderer2,
  ViewEncapsulation,
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { ContentService } from '../content.service';
import { createElement } from '../libs/element-creator';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-content-holder',
  templateUrl: './content-holder.component.html',
  styleUrls: ['./content-holder.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ContentHolderComponent implements OnInit {
  userName: string;
  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2,
    @Inject(DOCUMENT) private document,
    private contentService: ContentService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params) => { this.userName = params['user'] });
    const child = document.createElement('div');
    this.contentService.getContent().subscribe((data) => {
      data[0].content.forEach(element => {
        createElement(child, element);
      });
    });
    if (this.userName) {
      const welcomeSpan = document.createElement('h1');
      welcomeSpan.innerHTML = `Hello ${this.userName}`;
      child.appendChild(welcomeSpan);
    }

    this.renderer.appendChild(this.elementRef.nativeElement, child);
    this.renderer.addClass(this.elementRef.nativeElement, 'card');
  }
}
